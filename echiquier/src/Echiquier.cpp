#include "Echiquier.h"
#include <iostream>

using namespace std;

/** CONSTRUCTEURS **/
Echiquier::Echiquier() {
  for (int i = 0 ; i < 64 ; i++)
    m_cases[i] = NULL;
}

/** Recupere la piece situee sur une case du plateau **/
Piece* Echiquier::getPiece(int x, int y) {
    if ((x>=1) && (x<=8) && (y>=1) && (y<=8))
        return m_cases[(x-1)+8*(y-1)];

    return NULL;
}

/** Verifie qu'une valeur est comprise entre 2 bornes **/
bool valeurDansIntervalle(int val, int minimum, int maximum) {
    if((val >= minimum) && (val <= maximum))
        return true;
    else return false;
}

/** Place une piece sur l'echiquier aux coordonnees de la piece **/
bool Echiquier::placer(Piece* p) {
    if (p != NULL) {
        int colonne = p->x();
        int ligne = p->y();
        int position = (colonne-1) + (ligne-1) * 8;

        if((m_cases[position] == NULL)
            && (valeurDansIntervalle(colonne,1,8))
            && (valeurDansIntervalle(ligne,1,8))) {
            m_cases[position] = p;
            return true;
        }
    }

    return false;
}

/** Enleve la piece situee sur une case qui devient vide **/
Piece* Echiquier::enleverPiece(int x, int y) {
    if (getPiece(x, y) != NULL) {
        if ((valeurDansIntervalle(x,1,8))
        && (valeurDansIntervalle(y,1,8))) {
            Piece *tmp = m_cases[x-1 + (y-1) * 8];
            m_cases[x-1 + (y-1) * 8] = NULL;
            return tmp;
        }
    }

    return NULL;
}

/** Replace une pièce qui a ete enlevee du plateau, aux coordonnees de la piece **/
void Echiquier::remettrePiece( Piece *p ) {
    placer(p);
}

/** Repete l'affichage d'un caractere a l'écran le nombre de fois indique **/
void repeter(string car, int nb) {
    for (int i=0; i<nb;i++) cout << car;
}

/** Affiche l'echiquier a l'ecran **/
void Echiquier::affiche() {
    string coinHautGauche  = "\xe2\x94\x8c";
    string coinHautDroit   = "\xe2\x94\x90";
    string coinBasGauche   = "\xe2\x94\x94";
    string coinBasDroit    = "\xe2\x94\x98";

    string traitHorizontal = "\xe2\x94\x80";
    string traitVertical   = "\xe2\x94\x82";

    string traitTHaut      = "\xe2\x94\xac";
    string traitTBas       = "\xe2\x94\xb4";
    string traitTGauche    = "\xe2\x94\x9c";
    string traitTDroit     = "\xe2\x94\xa4";
    string traitCroix      = "\xe2\x94\xbc";

    // Haut du plateau
    cout << endl << "     1   2   3   4   5   6   7   8" << endl;
    cout << endl << "   " << coinHautGauche;
    repeter(traitHorizontal + traitHorizontal + traitHorizontal + traitTHaut, 7);
    cout << traitHorizontal + traitHorizontal + traitHorizontal + coinHautDroit << endl;

    // Intérieur du plateau
    for (int y = 1; y <= 8; ++y) {
        cout << y << "  ";

        for (int x = 1; x <= 8; ++x) {
            string c;
            Piece* p = getPiece(x, y);

            if (p == 0) c = ((x + y) % 2) == 0 ? '.' : ' ';
            else        c = p->codePiece();

            cout << traitVertical << " " << c << " ";
        }

        cout << traitVertical << "  " << y << endl;
        if (y != 8) {
            cout << "   " << traitTGauche;
            repeter(traitHorizontal + traitHorizontal + traitHorizontal + traitCroix, 7);
            cout << traitHorizontal + traitHorizontal + traitHorizontal + traitTDroit << endl;
        }
    }

    // Bas du plateau
    cout << "   " << coinBasGauche;
    repeter(traitHorizontal + traitHorizontal + traitHorizontal + traitTBas, 7);
    cout << traitHorizontal + traitHorizontal + traitHorizontal + coinBasDroit << endl;
    cout << "     1   2   3   4   5   6   7   8" << endl;
}
