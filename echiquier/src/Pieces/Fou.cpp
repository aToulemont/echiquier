#include "Fou.h"
#include "Piece.h"
#include <stdlib.h>


Fou::Fou(bool white, bool left) : Piece(left?3:6, white?1:8,white) { }

bool Fou::mouvementValide(Echiquier &e, int colDest, int ligneDest) {
    bool caseAccessible = Piece::caseAlliee(e, colDest, ligneDest);
    int colonneCourante = this->x(), ligneCourante = this->y();

    int directionLigne = sensDirection(ligneCourante, ligneDest);
    int directionColonne = sensDirection(colonneCourante, colDest);

    bool obstacle = false;
    int i = 1;

    if(caseAccessible) {
        // Vérification du mouvement autorisé
        if (abs(colDest-colonneCourante) != abs(ligneDest-ligneCourante))
            return false;

        // Obstacles sur le trajet
        while (i < abs(colDest-colonneCourante) && !obstacle){
            int colonneSuivante = colonneCourante + i * directionColonne;
            int ligneSuivante = ligneCourante + i * directionLigne;

            if (e.getPiece(colonneSuivante, ligneSuivante) != NULL)
                obstacle = true;

            i++;
        }
        return (!obstacle);
    }

    return false;
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Fou::casesBloquantes(Echiquier &e, int colDest, int ligneDest) {
    int colonneCourante = this->x();
    int ligneCourante = this->y();

    vector<Case*> itineraire;

    int directionLigne = sensDirection(ligneCourante, ligneDest);
    int directionColonne = sensDirection(colonneCourante, colDest);

    int i = 1;
    bool obstacle = false;

    // Obstacles sur le trajet
    while (i < abs(colDest-colonneCourante) && !obstacle){
        int ligne = ligneCourante + i * directionLigne;
        int colonne = colonneCourante + i * directionColonne;

        if (e.getPiece(colonne, ligne) != NULL)
            obstacle = true;
        else
            itineraire.push_back(new Case(colonne,ligne));

        i++;
    }

    return itineraire;
}

string Fou::codePiece() {
  return (m_white)?"\xe2\x99\x9d":"\xe2\x99\x97";
}
