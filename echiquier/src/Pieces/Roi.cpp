#include "Roi.h"
#include "Case.h"

#include <stdlib.h>
#include <iostream>

Roi::Roi(bool white) : Piece(5,white?1:8,white) { }

bool Roi::mouvementValide(Echiquier &e, int colDest, int ligneDest)
{
    bool caseAccessible = Piece::caseAlliee(e, colDest, ligneDest);
    int colonneCourante = this->x(), ligneCourante = this->y();

    // Distance maximale : 1 case
    if(caseAccessible && abs(colonneCourante-colDest) <= 1 && abs(ligneCourante-ligneDest) <= 1)
        return true;

    return false;
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Roi::casesBloquantes(Echiquier &e, int colDest, int ligneDest)
{
    vector<Case*> itineraire;

    int colonneCourante = this->x();
    int ligneCourante = this->y();

    // Distance maximale : 1 case
    if (abs(colonneCourante-colDest) <= 1 && abs(ligneCourante-ligneDest) <= 1)
        itineraire.push_back(new Case(colDest,ligneDest));

    return itineraire;
}

/** Retourne les pièces adverses qui mettent le roi en échec **/
vector<Piece*> Roi::echec(int colRoi, int ligneRoi, vector<Piece*> piecesAdverses, Echiquier &e)
{
    vector<Piece*> piecesEchec;
    vector<Piece*>::iterator pieceEnnemie = piecesAdverses.begin();

    // Pour chaque pièce adverse, on regarde si elles peuvent atteindre le roi
    while (pieceEnnemie != piecesAdverses.end())
    {
        if ((*pieceEnnemie)->mouvementValide(e, colRoi, ligneRoi))
            piecesEchec.push_back((*pieceEnnemie));

        pieceEnnemie++;
    }

    return piecesEchec;
}

/** Indique si le roi est en échec et mat ou non **/
bool Roi::echecEtMat(vector<Piece*> piecesAdverses, Echiquier &e)
{
    int colRoi = this->x(), ligneRoi = this->y();
    vector<Piece*>::iterator pieceEnnemie = piecesAdverses.begin();
    vector<Piece*> piecesEchec;
    vector<Case*> itineraire;

    // On parcours les cases autour du roi
    // On regarde si une case peut le sauver (où il ne sera pas mis en échec)
    int minLigne = ligneRoi-1;
    if (ligneRoi == 1) minLigne = 1;

    int maxLigne = ligneRoi+1;
    if (ligneRoi == 8) maxLigne = 8;

    int minCol = colRoi-1;
    if (colRoi == 1) minCol = 1;

    int maxCol = colRoi+1;
    if (colRoi == 8) maxCol = 8;

    for(int ligne = minLigne; ligne<=maxLigne; ligne++)
    {
        for (int colonne = minCol; colonne<=maxCol; colonne++)
        {
            // On ignore sa position actuelle
            if(colonne == colRoi && ligne == ligneRoi) continue;
            if(e.getPiece(colonne, ligne) != NULL) continue;

            // On regarde si la piece ennemie met le roi en echec
            piecesEchec = echec(colonne, ligne, piecesAdverses, e);

            if (piecesEchec.empty())
                return false;   // Le roi peut bouger

        }
    }
    // Si plus d'une pièce met le roi en échec, il a perdu.
    piecesEchec.clear();
    piecesEchec = echec(colRoi, ligneRoi, piecesAdverses, e);
    if (piecesEchec.size() > 1 || piecesEchec.empty()) return true;

    // Si on peut manger la pièce qui met le roi en échec
    while(pieceEnnemie != piecesAdverses.end())
    {
        if((*pieceEnnemie)->mouvementValide(e, piecesEchec[0]->x(), piecesEchec[0]->y()))
        {
            return false;
        }
        pieceEnnemie++;
    }

    // Si un cavalier ou un pion mettent le roi en échec et mat, le roi a perdu
    // -> pas possible de mettre une pièce bloquant leur chemin
    string codePiece = piecesEchec[0]->codePiece();

    // Cavalier
    if (codePiece == "\xe2\x99\x9e" || codePiece == "\xe2\x99\x98") return true;

    // Pion
    if (codePiece == "\xe2\x99\x9f" || codePiece == "\xe2\x99\x99") return true;


    // on regarde toutes les cases qu'il y a entre le roi et la pièce attaquante,
    // si aucune pièce peut se mettre sur le chemin : echec et mat
    if (!piecesEchec.empty())
    {
        itineraire = piecesEchec[0]->casesBloquantes(e, colRoi, ligneRoi);
    }

    if (!itineraire.empty() && !piecesAdverses.empty())
    {
        pieceEnnemie = piecesAdverses.begin();
        vector<Case*>::iterator c = itineraire.begin();
        while(pieceEnnemie != piecesAdverses.end())
        {
            while(c != itineraire.end())
            {
                if((*pieceEnnemie)->mouvementValide(e, (*c)->x(), (*c)->y()))
                {
                    return true;
                }
                c++;
            }
            pieceEnnemie++;
        }
    }
    return false;
}

string Roi::codePiece()
{
    return (m_white)?"\xe2\x99\x9a":"\xe2\x99\x94";
}
