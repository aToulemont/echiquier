#include "Tour.h"
#include <stdlib.h>
#include <iostream>

Tour::Tour(bool white, bool left) : Piece(left?1:8, white?1:8,white) { }

bool Tour::mouvementValide(Echiquier &e, int colDest, int ligneDest) {
    bool caseAccessible = Piece::caseAlliee(e, colDest, ligneDest);

    int colonneCourante = this->x(), ligneCourante = this->y();

    int directionLigne = sensDirection(ligneCourante, ligneDest);
    int directionColonne = sensDirection(colonneCourante, colDest);

    int i = 1;
    int nbDeplacements;
    bool obstacle = false;

    if(caseAccessible) {
        // Vérification du mouvement autorisé
        if (colonneCourante != colDest && ligneCourante != ligneDest)
            return false;

        if(colonneCourante != colDest)
            nbDeplacements = abs(colonneCourante - colDest);
        else nbDeplacements = abs(ligneCourante - ligneDest);

        // Vérification des obstacles sur le trajet
        while (i < nbDeplacements){
            if (colonneCourante != colDest) {
                int colonneSuivante = colonneCourante + i * directionColonne;
                if (e.getPiece(colonneSuivante, ligneCourante) != NULL)
                    obstacle = true;
            }

            if (ligneCourante != ligneDest) {
                int ligneSuivante = ligneCourante + i * directionLigne;
                if (e.getPiece(colonneCourante, ligneSuivante) != NULL)
                    obstacle = true;
            }

            i++;
        }
        return (!obstacle);
    }

    return false;
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Tour::casesBloquantes(Echiquier &e, int colDest, int ligneDest) {
    int colonneCourante = this->x(), ligneCourante = this->y();
    vector<Case*> itineraire;

    int directionLigne = sensDirection(ligneCourante, colDest);
    int directionColonne = sensDirection(colonneCourante, ligneDest);

    int nbDeplacements;
    if(colonneCourante == colDest) nbDeplacements = abs(ligneCourante-ligneDest);
    else nbDeplacements = abs(colonneCourante-colDest);

    int i = 1;

    while (i < nbDeplacements){
        if (colonneCourante != colDest)
            itineraire.push_back(new Case(colonneCourante + i * directionColonne,ligneCourante));

        if (ligneCourante != ligneDest)
            itineraire.push_back(new Case(colonneCourante,ligneCourante + i * directionLigne));

        i++;
    }

    return itineraire;
}

string Tour::codePiece() {
  return (m_white)?"\xe2\x99\x9c":"\xe2\x99\x96";
}
