#include "Pion.h"

#include <iostream>
#include <stdlib.h>
using namespace std;

Pion::Pion(bool white, int x) : Piece(x, white ? 2 : 7,white) { }

bool Pion::mouvementValide(Echiquier &e, int colDest, int ligneDest) {
    bool caseAccessible = Piece::caseAlliee(e, colDest, ligneDest);

    int direction = 1;
    if(isBlack()) direction = -1;

    // Ligne de référence d'origine des pions
    int ligneOrigine = isWhite() ? 2 : 7;
    int colonneCourante = this->x(), ligneCourante = this->y();

    // Distance parcourue par le pion
    int distance = direction * (ligneDest - ligneCourante);

    // Case de destination accessible
    if(caseAccessible && e.getPiece(colDest,ligneDest) == NULL) {
        // Le pion avance d'une case
        if(colonneCourante == colDest && distance == 1) {
            return true;
        }

        // Le pion avance de 2 cases (doit n'avoir jamais bougé)
        if(ligneOrigine == ligneCourante && colonneCourante == colDest && distance == 2) {
            int ligneCaseSuivante = ligneCourante+1 * direction;
            return (e.getPiece(colonneCourante, ligneCaseSuivante) == NULL);
        }
    }

    //Diagonales (si pièce adverse)
    if(e.getPiece(colDest,ligneDest) != NULL) {
        if( distance == 1 && abs(colDest-colonneCourante) == 1)
            return (e.getPiece(colDest,ligneDest)->isBlack() != isBlack());
    }


    return false;
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Pion::casesBloquantes(Echiquier &e, int colDest, int ligneDest) {
    // Cases parcourues par la pièce
    vector<Case*> itineraire;

    int direction = 1;
    if(isBlack()) direction = -1;

    // Ligne de référence d'origine du pion
    int ligneOrigine = isWhite() ? 2 : 7;
    int colonneCourante = this->x(), ligneCourante = this->y();

    // Distance parcourue par le pion
    int distance = direction * (ligneDest - ligneCourante);

    // Si la case de destination est vide
    if(e.getPiece(colDest,ligneDest) == NULL) {

        // Le pion avance d'une case
        if(colonneCourante == colDest && distance == 1)
            itineraire.push_back(new Case(colonneCourante,ligneDest));

        // Le pion avance de 2 cases (doit n'avoir jamais bougé)
        if(colonneCourante == colDest && ligneOrigine == ligneCourante && distance == 2) {
            // Si pas d'obstable
            int ligneCaseSuivante = ligneCourante+1 * direction;
            if (e.getPiece(colonneCourante, ligneCaseSuivante) == NULL) {
                itineraire.push_back(new Case(colonneCourante, ligneCaseSuivante));
                itineraire.push_back(new Case(colonneCourante, ligneDest));
            }
        }
    }

    // Diagonale (si pièce advserse)
    if(e.getPiece(colDest,ligneDest) != NULL && distance == 1 && abs(colDest-colonneCourante) == 1) {
        if (e.getPiece(colDest,ligneDest)->isBlack() != isBlack())
            itineraire.push_back(new Case(colDest,ligneDest));
    }

    return itineraire;
}

string Pion::codePiece() {
  return (m_white) ? "\xe2\x99\x9f" : "\xe2\x99\x99";
}
