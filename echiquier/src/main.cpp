#include "Jeu.h"

/**
 * Programme principal
 */
int main( int argc, char** argv ) {
    Jeu j;
    j.init();
    j.majAffichage();
    j.jeu();
}
