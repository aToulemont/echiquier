#include "Test.cpp"

/**
 * Programme des tests de déplacement des pièces
 */
int main( int argc, char** argv ) {
    cout << "-----------------" << endl << " TEST DES PIECES " << endl << "-----------------" << endl << endl;
    Test test;
    test.testPion();
    test.testTour();
    test.testCavalier();
    test.testFou();
    test.testReine();
    test.testRoi();
}
