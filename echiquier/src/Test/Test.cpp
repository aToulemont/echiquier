#include "Echiquier.h"
#include "Joueur.h"
#include "Piece.h"
#include "Reine.h"
#include "Pion.h"
#include "Tour.h"
#include "Fou.h"
#include "Cavalier.h"

#include <iostream>

using namespace std;

class Test {
public:
    void testAvancerUneCasePossiblePion() {
        cout << "[Avancer d'une case] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(pb1->mouvementValide(e,2,3)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAvancerDeuxCasesPossiblePion() {
        cout << "[Avancer de deux cases au depart possible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(pb1->mouvementValide(e,2,4)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossiblePion() {
        cout << "[Manger en diagonale : possible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        Piece* pn1= new Pion(false,0);

        pb1->init(2,2,true); e.placer(pb1);
        pn1->init(1,3, false); e.placer(pn1);

        if(pb1->mouvementValide(e,1,3)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAllerSurCaseNonPrevueImpossiblePion() {
        cout << "[Avancer d'une case sur le cote : impossible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(!pb1->mouvementValide(e,3,2)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCaseSurPlaceImpossiblePion() {
        cout << "[Faire du sur place : impossible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(!pb1->mouvementValide(e,2,2)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossiblePion() {
        cout << "[Manger allie : impossible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        Piece* pb2= new Pion(true,0);

        pb1->init(2,2,true); e.placer(pb1);
        pb2->init(3,3,true);e.placer(pb2);

        if(!pb1->mouvementValide(e,3,3)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAvancerDeuxCasesApresDepartImpossiblePion() {
        cout << "[Avancer de deux cases apres le depart : impossible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(!pb1->mouvementValide(e,3,5)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testReculerImpossiblePion() {
        cout << "[reculer impossible] ";

        Echiquier e;
        Piece* pb1= new Pion(true,1);
        pb1->init(2,2,true); e.placer(pb1);

        if(!pb1->mouvementValide(e,2,1)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testPion(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : PION" << endl;

        testAvancerUneCasePossiblePion();
        testAvancerDeuxCasesPossiblePion();
        testMangerEnnemiPossiblePion();
        testAllerSurCaseNonPrevueImpossiblePion();
        testCaseSurPlaceImpossiblePion();
        testMangerAllieImpossiblePion();
        testAvancerDeuxCasesApresDepartImpossiblePion();
        testReculerImpossiblePion();

        cout << endl << "-------------------------------------------" << endl;
    }

    /** ------------------------------------- **/

    void testAvancerCaseVidePossibleTour() {
        cout << "[Avancer dans toutes les directions] ";

        Echiquier e;
        Piece* t = new Tour(true,true);
        t->init(4,4,true); e.placer(t);

        if(t->mouvementValide(e,4,1)
           && t->mouvementValide(e,4,8)
           && t->mouvementValide(e,1,4)
           && t->mouvementValide(e,8,4)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAllerSurCaseNonPrevueImpossibleTour() {
        cout << "[Avancer sur une case non prevue : impossible] ";

        Echiquier e;
        Piece* t = new Tour(true,true);
        t->init(4,4,true); e.placer(t);

        if(!t->mouvementValide(e,1,1)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossibleTour() {
        cout << "[Manger un ennemi] ";

        Echiquier e;
        Piece* t = new Tour(true,true);
        Piece* p = new Pion(false,0);

        t->init(4,4,true); e.placer(t);
        p->init(4,7,false); e.placer(p);

        if(t->mouvementValide(e,4,5)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossibleTour() {
        cout << "[Manger un allie : impossible] ";

        Echiquier e;
        Piece* t = new Tour(true,true);
        Piece* pb = new Pion(true,1);

        t->init(4,4,true); e.placer(t);
        pb->init(4,6,true); e.placer(pb);

        if(!t->mouvementValide(e,4,6)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAvecObstacleImpossibleTour() {
        cout << "[Manger avec obstacle : impossible] ";

        Echiquier e;
        Piece* t = new Tour(true,true);
        Piece* pb = new Pion(true,1);

        t->init(4,4,true); e.placer(t);
        pb->init(4,6,true); e.placer(pb);

        if(!t->mouvementValide(e,4,7)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testTour(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : TOUR" << endl;

        testAvancerCaseVidePossibleTour();
        testAllerSurCaseNonPrevueImpossibleTour();
        testMangerEnnemiPossibleTour();
        testMangerAllieImpossibleTour();
        testMangerAvecObstacleImpossibleTour();

        cout << endl << "-------------------------------------------" << endl;
    }

    /** ------------------------------------- **/

    void testAvancerCaseVidePossibleCavalier() {
        cout << "[Avancer dans une case vide] ";

        Echiquier e;
        Piece* x = new Cavalier(true,true);
        x->init(4,4,true); e.placer(x);

        if(
           x->mouvementValide(e,5,2) &&
           x->mouvementValide(e,6,3) &&
           x->mouvementValide(e,6,5) &&
           x->mouvementValide(e,5,6) &&
           x->mouvementValide(e,3,6) &&
           x->mouvementValide(e,2,5) &&
           x->mouvementValide(e,2,3) &&
           x->mouvementValide(e,3,2)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossibleCavalier() {
        cout << "[Avancer en mangeant un ennemi] ";

        Echiquier e;
        Piece* x = new Cavalier(true,true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(5,2,false); e.placer(p);
        p->init(6,3,false); e.placer(p);
        p->init(6,5,false); e.placer(p);
        p->init(5,6,false); e.placer(p);
        p->init(3,6,false); e.placer(p);
        p->init(2,5,false); e.placer(p);
        p->init(2,3,false); e.placer(p);
        p->init(3,2,false); e.placer(p);
        if(
           x->mouvementValide(e,5,2) &&
           x->mouvementValide(e,6,3) &&
           x->mouvementValide(e,6,5) &&
           x->mouvementValide(e,5,6) &&
           x->mouvementValide(e,3,6) &&
           x->mouvementValide(e,2,5) &&
           x->mouvementValide(e,2,3) &&
           x->mouvementValide(e,3,2)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossibleCavalier() {
        cout << "[Avancer en mangeant un allie : impossible] ";

        Echiquier e;
        Piece* x = new Cavalier(true,true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(5,2,true); e.placer(p);

        if( !x->mouvementValide(e,5,2) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCaseSurPlaceImpossibleCavalier() {
        cout << "[Faire du sur place : impossible] ";

        Echiquier e;
        Piece* x = new Cavalier(true,true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,4,4) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAllerSurCaseNonPrevueImpossibleCavalier() {
        cout << "[Aller sur une case non prevue : impossible] ";

        Echiquier e;
        Piece* x = new Cavalier(true,true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,5,5) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCavalier(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : CAVALIER" << endl;

        testAvancerCaseVidePossibleCavalier();
        testMangerEnnemiPossibleCavalier();
        testMangerAllieImpossibleCavalier();
        testCaseSurPlaceImpossibleCavalier();
        testAllerSurCaseNonPrevueImpossibleCavalier();

        cout << endl << "-------------------------------------------" << endl;
    }

    /** ------------------------------------- **/

    void testAvancerCaseVidePossibleFou() {
        cout << "[Avancer dans une case vide] ";

        Echiquier e;
        Piece* x = new Fou(true,true);
        x->init(4,4,true); e.placer(x);

        if(
           x->mouvementValide(e,1,1) &&
           x->mouvementValide(e,2,2) &&
           x->mouvementValide(e,3,3) &&
           x->mouvementValide(e,5,5) &&
           x->mouvementValide(e,6,6) &&
           x->mouvementValide(e,7,7) &&
           x->mouvementValide(e,8,8) &&
           x->mouvementValide(e,7,1) &&
           x->mouvementValide(e,6,2) &&
           x->mouvementValide(e,3,5) &&
           x->mouvementValide(e,2,6) &&
           x->mouvementValide(e,7,1)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossibleFou() {
        cout << "[Manger un ennemi] ";

        Echiquier e;
        Piece* x = new Fou(true,true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(0,0,false); e.placer(p);
        p->init(6,6,false); e.placer(p);
        p->init(6,2,false); e.placer(p);
        p->init(1,7,false); e.placer(p);

        if(
           x->mouvementValide(e,0,0) &&
           x->mouvementValide(e,6,6) &&
           x->mouvementValide(e,6,2) &&
           x->mouvementValide(e,1,7)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossibleFou() {
        cout << "[Manger un allie : impossible] ";

        Echiquier e;
        Piece* x = new Fou(true,true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(6,2,true); e.placer(p);

        if(!x->mouvementValide(e,6,2)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCaseSurPlaceImpossibleFou() {
        cout << "[Faire du sur place : impossible] ";

        Echiquier e;
        Piece* x = new Fou(true,true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,4,4) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAllerSurCaseNonPrevueImpossibleFou() {
        cout << "[Aller sur une case non prevue : impossible] ";

        Echiquier e;
        Piece* x = new Fou(true,true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,5,6) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testFou(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : FOU" << endl;

        testAvancerCaseVidePossibleFou();
        testMangerEnnemiPossibleFou();
        testMangerAllieImpossibleFou();
        testCaseSurPlaceImpossibleFou();
        testAllerSurCaseNonPrevueImpossibleFou();

        cout << endl << "-------------------------------------------" << endl;
    }

    /** ------------------------------------- **/

    void testAvancerCaseVidePossibleReine() {
        cout << "[Avancer dans une case vide] ";

        Echiquier e;
        Piece* x = new Reine(true);
        x->init(4,4,true); e.placer(x);

        if(
           x->mouvementValide(e,1,1) &&
           x->mouvementValide(e,2,2) &&
           x->mouvementValide(e,3,3) &&
           x->mouvementValide(e,5,5) &&
           x->mouvementValide(e,6,6) &&
           x->mouvementValide(e,7,7) &&
           x->mouvementValide(e,8,8) &&
           x->mouvementValide(e,7,1) &&
           x->mouvementValide(e,6,2) &&
           x->mouvementValide(e,5,3) &&
           x->mouvementValide(e,3,5) &&
           x->mouvementValide(e,2,6) &&
           x->mouvementValide(e,1,7) &&
           x->mouvementValide(e,4,2) &&
           x->mouvementValide(e,4,5) &&
           x->mouvementValide(e,6,4) &&
           x->mouvementValide(e,4,6)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossibleReine() {
        cout << "[Manger un ennemi] ";

        Echiquier e;
        Piece* x = new Reine(true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(1,1,false); e.placer(p);
        p->init(6,6,false); e.placer(p);
        p->init(6,2,false); e.placer(p);
        p->init(1,7,false); e.placer(p);
        p->init(4,2,false); e.placer(p);
        p->init(4,5,false); e.placer(p);
        p->init(1,4,false); e.placer(p);
        p->init(6,4,false); e.placer(p);
        if(
           x->mouvementValide(e,1,1) &&
           x->mouvementValide(e,6,6) &&
           x->mouvementValide(e,6,2) &&
           x->mouvementValide(e,1,7) &&
           x->mouvementValide(e,4,2) &&
           x->mouvementValide(e,4,5) &&
           x->mouvementValide(e,1,4) &&
           x->mouvementValide(e,6,4)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossibleReine() {
        cout << "[Manger un allie : impossible] ";

        Echiquier e;
        Piece* x = new Reine(true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(6,6,true); e.placer(p);
        p->init(6,4,true); e.placer(p);

        if(!x->mouvementValide(e,6,4) && !x->mouvementValide(e,6,6)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCaseSurPlaceImpossibleReine() {
        cout << "[Faire du sur place : impossible] ";

        Echiquier e;
        Piece* x = new Reine(true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,4,4) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAvecObstacleImpossibleReine() {
        cout << "[Manger avec obstacle : impossible] ";

        Echiquier e;
        Piece* x = new Reine(true);
        Piece* p = new Pion(false,0);
        Piece* pb = new Pion(false,5);

        x->init(4,4,true); e.placer(x);
        p->init(6,4,false); e.placer(p);
        pb->init(5,4,true); e.placer(pb);

        if(!x->mouvementValide(e,6,4) && !x->mouvementValide(e,5,4) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testAllerSurCaseNonPrevueImpossibleReine() {
        cout << "[Aller sur une case non prevue : impossible] ";

        Echiquier e;
        Piece* x = new Reine(true);
        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,5,2) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testReine(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : REINE" << endl;

        testAvancerCaseVidePossibleReine();
        testMangerEnnemiPossibleReine();
        testMangerAllieImpossibleReine();
        testCaseSurPlaceImpossibleReine();
        testMangerAvecObstacleImpossibleReine();
        testAllerSurCaseNonPrevueImpossibleReine();

        cout << endl << "-------------------------------------------" << endl;
    }


    /** ------------------------------------- **/

    void testAvancerCaseVidePossibleRoi() {
        cout << "[Avancer dans une case vide] ";

        Echiquier e;
        Roi* x = new Roi(true);
        x->init(4,4,true); e.placer(x);

        if(
           x->mouvementValide(e,3,3) &&
           x->mouvementValide(e,3,4) &&
           x->mouvementValide(e,3,5) &&
           x->mouvementValide(e,4,3) &&
           x->mouvementValide(e,4,5) &&
           x->mouvementValide(e,5,3) &&
           x->mouvementValide(e,5,4) &&
           x->mouvementValide(e,5,5)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerEnnemiPossibleRoi() {
        cout << "[Manger un ennemi] ";

        Echiquier e;
        Roi* x = new Roi(true);
        Piece* p = new Pion(false,0);
        x->init(4,4,true); e.placer(x);

        p->init(3,3,false); e.placer(p);
        p->init(3,4,false); e.placer(p);
        p->init(3,5,false); e.placer(p);
        p->init(4,3,false); e.placer(p);
        p->init(4,5,false); e.placer(p);
        p->init(5,3,false); e.placer(p);
        p->init(5,4,false); e.placer(p);
        p->init(5,5,false); e.placer(p);
        if(
           x->mouvementValide(e,3,3) &&
           x->mouvementValide(e,3,4) &&
           x->mouvementValide(e,3,5) &&
           x->mouvementValide(e,4,3) &&
           x->mouvementValide(e,4,5) &&
           x->mouvementValide(e,5,3) &&
           x->mouvementValide(e,5,4) &&
           x->mouvementValide(e,5,5)
           ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testMangerAllieImpossibleRoi() {
        cout << "[Manger un allie : impossible] ";

        Echiquier e;
        Roi* x = new Roi(true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);
        p->init(3,3,true); e.placer(p);

        if(!x->mouvementValide(e,3,3)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testCaseSurPlaceImpossibleRoi() {
        cout << "[Faire du sur place : impossible] ";

        Echiquier e;
        Roi* x = new Roi(true);
        Piece* p = new Pion(false,0);

        x->init(4,4,true); e.placer(x);

        if( !x->mouvementValide(e,4,4) ) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testEtreEchecEtMatPossibleRoi() {
        cout << "[Etre en echec et mat] ";

        Echiquier e;
        Roi* x = new Roi(true);
        Piece* p = new Pion(false,0);
        x->init(4,4,true); e.placer(x);

        // Blanc
        Piece* F = new Fou(true,true);
        Piece* R = new Reine(true);
        Piece* Cleft = new Cavalier(true, true);
        Piece* Cright = new Cavalier(true, false);

        // Noir
        Piece* Fblack = new Fou(false,false);
        Piece* p1 = new Pion(false,4);
        Piece* p2 = new Pion(false,5);

        x->init(5,8,false);         e.placer(x);
        F->init(8,3,true);          e.placer(F);
        Fblack->init(6,8,false);    e.placer(Fblack);
        R->init(1,5,true);          e.placer(R);
        Cleft->init(2,7,true);      e.placer(Cleft);
        Cright->init(3,7,true);     e.placer(Cright);
        p1->init(5,7,false);        e.placer(p1);
        p2->init(6,7,false);        e.placer(p2);

        vector<Piece*> piecesJoueurAdvers;

        piecesJoueurAdvers.push_back(x);
        piecesJoueurAdvers.push_back(F);
        piecesJoueurAdvers.push_back(R);
        piecesJoueurAdvers.push_back(Cleft);
        piecesJoueurAdvers.push_back(Cright);
        piecesJoueurAdvers.push_back(p1);
        piecesJoueurAdvers.push_back(p2);

        if(x->echecEtMat(piecesJoueurAdvers, e)) cout << "OK" << endl;
        else cout << "ERR" << endl;
    }

    void testRoi(){
        cout << endl << "-------------------------------------------" << endl;
        cout << "MOUVEMENT-VALIDE : ROI" << endl << endl;

        testAvancerCaseVidePossibleRoi();
        testMangerEnnemiPossibleRoi();
        testMangerAllieImpossibleRoi();
        testCaseSurPlaceImpossibleRoi();
        testEtreEchecEtMatPossibleRoi();

        cout << endl << "-------------------------------------------" << endl;
    }
};
