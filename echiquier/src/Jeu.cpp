#include "Jeu.h"

Jeu::Jeu()
{
    //ctor
}

Jeu::~Jeu()
{
    //dtor
}

void Jeu::init() {
    jblanc.placerPieces(e);
    jnoir.placerPieces(e);
    jBlancActif = true;
}

void Jeu::majAffichage() {
    system("clear");
    e.affiche();
}

void Jeu::saisieCoordonnee(string texte, int* valeur) {
    string saisie;
    cout << texte << " : ";
    cin >> saisie;
    *valeur = atoi(saisie.c_str());

    while(*valeur < 1 || *valeur > 8){
        majAffichage();
        cout << "Valeur incorrect, entrez un nombre entre 1 et 8." << endl;
        cout << texte << " : ";
        cin >> saisie;
        *valeur = atoi(saisie.c_str());
    }
}


Piece * Jeu::pieceABouger() {
    int ligne, colonne;
    bool pieceValide = false;
    Piece *p;

    while (!pieceValide) {
        /* Saisies des coordonnées */
        saisieCoordonnee("Ligne", &ligne);
        saisieCoordonnee("Colonne", &colonne);

        /* Récupération de la pièce */
        p = e.getPiece(colonne, ligne);

        if (p != NULL) {
            if (jBlancActif) {
                if (jblanc.piecePresente(*p)) pieceValide = true;
                else pieceValide = false;
            }
            else {
                if (jnoir.piecePresente(*p)) pieceValide = true;
                else pieceValide = false;
            }
        }

        if (!pieceValide) {
            majAffichage();
            cout << endl << "Choisissez une de vos pièces sur le plateau." << endl;
        }
    }

    return p;
}

Case Jeu::caseDestination(Case c) {
    int ligne, colonne;
    bool caseValide = false;

    while (!caseValide) {
        cout << "Où voulez-vous la placer ?" << endl;
        saisieCoordonnee("Ligne", &ligne);
        saisieCoordonnee("Colonne", &colonne);

        if (ligne != c.y() || colonne != c.x()) caseValide = true;
        else cout << "Choisissez une case différente de celle de la pièce sélectionnée" << endl;
    }

    return (Case(colonne, ligne));
}

void Jeu::jeu() {
    string joueurCourant;
    bool echec = false;
    bool perdu = false;
    bool tourTermine = false;

    while (!perdu) {
        while (!tourTermine) {
            joueurCourant = jBlancActif ? "BLANC" : "NOIR";
            cout << endl << "............" << endl
            << joueurCourant << " : quelle pièce voulez-vous bouger ?" << endl;

            Piece* p;
            p = pieceABouger();

            cout << "Pièce choisie" << endl;
            Case casePieceChoisie = Case(p->x(), p->y());
            Case caseDest = caseDestination(casePieceChoisie);

            int colonne = p->x();
            int ligne = p->y();

            int colonneDest = caseDest.x();
            int ligneDest = caseDest.y();

            /* Vérification du mouvement valide */
            if (p->mouvementValide(e, colonneDest, ligneDest)) {

                /* On enlève la pièce qui est sur la case de destination */
                Piece* enleveeCaseDest;
                enleveeCaseDest = e.enleverPiece(colonneDest, ligneDest);

                if (jBlancActif) jnoir.enleverPiece(*enleveeCaseDest);
                else jblanc.enleverPiece(*enleveeCaseDest);

                /* MAJ des coordonnées de la pièce sélectionnée */
                int ligneOrigine = p->y();
                int colonneOrigine = p->x();
                p->move(colonneDest, ligneDest);

                /* Déplacement sur l'échiquier */
                e.placer(p);

                /* Suppression de la pièce de sa case d'origine */
                Piece* enleveeCaseOrigine;
                enleveeCaseOrigine = e.enleverPiece(colonne, ligne);


                /* Le joueur courant se met en échec ? */
                Roi* r = !jBlancActif ? jnoir.getRoi() : jblanc.getRoi();
                vector<Piece*> piecesJoueurAdvers = !jBlancActif ? jblanc.getPieces() : jnoir.getPieces();

                if (!r->echec(r->x(), r->y(), piecesJoueurAdvers, e).empty()) {
                    tourTermine = false;

                    // Oui, echec, on annule le mouvement
                    e.remettrePiece(enleveeCaseDest);

                    if (jBlancActif) jnoir.remettrePiece(enleveeCaseDest);
                    else             jblanc.remettrePiece(enleveeCaseDest);

                    // On remet à sa place la pièce déplacée
                    p->move(colonneOrigine, ligneOrigine);
                    e.remettrePiece(enleveeCaseOrigine);

                    // On supprime la pièce qu'on avait déplacé
                    e.enleverPiece(colonneDest, ligneDest);

                    majAffichage();

                    if (echec) {
                        cout << endl << "Vous ne pouvez pas rester en échec. Rejouez : " << endl;
                    } else {
                        cout << endl << "Vous ne pouvez pas vous mettre en échec. Rejouez : " << endl;
                    }
                } else {
                    tourTermine = true;

                    /* Le roi adverse est en échec ? */
                    Roi* r = jBlancActif ? jnoir.getRoi() : jblanc.getRoi();
                    vector<Piece*> piecesJoueurActif = jBlancActif ? jblanc.getPieces() : jnoir.getPieces();
                    vector<Piece*> piecesJoueurAdvers = !jBlancActif ? jblanc.getPieces() : jnoir.getPieces();

                    // Si au moins une pièce met le roi en échec
                    if (!r->echec(r->x(), r->y(), piecesJoueurActif, e).empty()) {
                        echec = true;

                        if(r->echecEtMat(piecesJoueurActif, e)) {
                            perdu = true;
                        }
                    } else {
                        echec = false;
                    }
                }

             } else {
                majAffichage();
                cout << endl << "Le mouvement n'est pas valide. Rejouez : " << endl;
            }
        }

        // Changement Joueur
        jBlancActif = !jBlancActif;

        majAffichage();

        if (echec && !perdu) {
            string roiEchec = jBlancActif ? "BLANC" : "NOIR";
            cout << endl << "Roi " << roiEchec << " en echec" << endl;
        }

        tourTermine = false;
    }

    /* Jeu terminé */
    joueurCourant = jBlancActif ? "BLANC" : "NOIR";
    cout << "Terminé. Vainqueur : " << joueurCourant << endl;
}
