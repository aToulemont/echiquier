#ifndef PION_H
#define PION_H

#include "Piece.h"

class Pion : public Piece {
    public:
        Pion(bool white, int x);
        bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
        vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
        std::string codePiece();
};

#endif // PION_H
