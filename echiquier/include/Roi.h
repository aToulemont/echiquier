#ifndef ROI_H
#define ROI_H

#include "Piece.h"

class Roi : public Piece {
    public:
        Roi(bool white);
        bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
        vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
        vector<Piece*> echec(int colRoi, int ligneRoi, vector<Piece*> pieces, Echiquier &e);
        bool echecEtMat(vector<Piece*> piecesAdverses, Echiquier &e);
        std::string codePiece();
};

#endif // ROI_H
