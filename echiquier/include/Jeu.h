#ifndef JEU_H
#define JEU_H

#include "Echiquier.h"
#include "Joueur.h"
#include "Piece.h"
#include "Reine.h"
#include "Pion.h"
#include "Tour.h"
#include "Fou.h"
#include "Cavalier.h"
#include "Case.h"

#include <iostream>

class Jeu {
    public:
        Jeu();
        virtual ~Jeu();
        void init();
        void majAffichage();
        void saisieCoordonnee(string texte, int* valeur);
        Piece * pieceABouger();
        Case caseDestination(Case c);
        void jeu();

        Echiquier e;
        JoueurBlanc jblanc;
        JoueurNoir jnoir;
        bool jBlancActif;
};

#endif // JEU_H
