#ifndef FOU_H
#define FOU_H

#include "Piece.h"

class Fou : virtual public Piece
{
    public:
        Fou(bool white, bool left);
        bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
        vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
        std::string codePiece();
};

#endif // FOU_H
