#ifndef CASE_H
#define CASE_H


class Case
{
    public:
        Case();
        Case(int x, int y);
        virtual ~Case();
        int x();
        int y();

    private:
        int m_x;
        int m_y;
};



#endif // CASE_H
