#ifndef PION_H
#define PION_H

#include "Piece.h"

class Pion : public Piece
{
public:
    Pion(bool white, int x);
    std::string toString();
    bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
    vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
    std::string codePiece();
    int getCoordXSprite();
};

#endif // PION_H
