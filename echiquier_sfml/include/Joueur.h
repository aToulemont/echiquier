#ifndef JOUEUR_H
#define JOUEUR_H

#include <vector>
#include <algorithm>
#include "Echiquier.h"
#include "Piece.h"
#include "Roi.h"

using namespace std;

class Joueur
{
public:
    Joueur();
    Joueur(bool white);
    ~Joueur();
    void affiche();
    bool isWhite();
    void placerPieces(Echiquier & e);
    bool piecePresente(Piece & p);
    void enleverPiece(Piece &p);
    void remettrePiece(Piece *p);
    Roi* getRoi();
    vector<Piece*> getPieces();

protected:
    vector<Piece*> m_pieces;

private:
    bool couleur;
};

class JoueurBlanc : public Joueur
{
public:
    JoueurBlanc();
};

class JoueurNoir : public Joueur
{
public:
    JoueurNoir();
};

#endif // JOUEUR_H
