#ifndef REINE_H
#define REINE_H

#include "Tour.h"
#include "Fou.h"

class Reine : virtual public Tour, virtual public Fou
{
public:
    Reine(bool white);
    std::string toString();
    bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
    vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
    std::string codePiece();
    int getCoordXSprite();
};

#endif // REINE_H
