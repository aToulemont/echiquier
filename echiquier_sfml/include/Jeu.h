#ifndef JEU_H
#define JEU_H

#include "Echiquier.h"
#include "Joueur.h"
#include "Piece.h"
#include "Case.h"
#include <SFML/Graphics.hpp>

#include <iostream>

class Jeu
{
public:
    Jeu();
    virtual ~Jeu();
    sf::RenderWindow* getWindow();
    void init();
    void initFonts();
    void initTextes();
    void initWindow();
    void saisieCoordonnee(string texte, int* valeur);
    Piece * pieceABouger();
    Case caseDestination(Case c);
    void menu();
    int eventQuitter(sf::Event event);
    void enrCaseSelectionnee(Piece *p, int colonneCourante, int ligneCourante);
    void verifMiseEnEchec(Piece *enleveeCaseDest, Piece *enleveeCaseOrigine, int colonneCourante, int ligneCourante);
    void mouvement(int colonneCourante, int ligneCourante);
    void terminerTour();
    void majAffichage();
    void affichageVainqueur();
    void jeu();

private:
    Echiquier e;
    JoueurBlanc jblanc;
    JoueurNoir jnoir;
    string joueurCourant;
    bool perdu = false;
    bool jBlancActif = true;
    bool tourTermine = false;
    bool echec = false;
    bool mouseClicked = false;
    bool mouvementValide = true;

    Piece* pSelectionnee = NULL;
    int ligneSelectionnee, colonneSelectionnee;
    int xSouris, ySouris;

    sf::Font littlelo;

    sf::Text titre;
    sf::Text continuer;
    sf::Text selectionneText;
    sf::Text joueurCourantText;
    sf::Text mouvementInvalideText;
    sf::Text echecText;
    sf::Text quitterText;
    sf::Text vainqueurText;

    sf::RenderWindow *window;
};

#endif // JEU_H
