/**
 * Header de Piece.cxx
 *
 * @file Piece.h
 */

#if !defined Piece_h
#define Piece_h

#include <vector>
#include <string>
#include "Case.h"
#include <SFML/Graphics.hpp>

using namespace std;

class Echiquier;

/**
 * Declaration d'une classe modélisant une piece de jeu d'echec.
 */
class Piece
{
protected:
    int m_x;
    int m_y;
    bool m_white;
    const int widthSprite = 64;

public:
    Piece();
    virtual ~Piece();
    Piece( int x, int y, bool white );
    Piece & operator=(const Piece & autre);
    virtual std::string toString();
    void init( int x, int y, bool white );
    void move( int x, int y );
    int x();
    int y();
    bool isWhite();
    bool isBlack();
    int getWidthSprite();
    void affiche();
    int sensDirection (int origine, int dest);
    virtual bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
    virtual vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
    bool caseAlliee(Echiquier &e, int x, int y);
    virtual std::string codePiece();
    virtual int getCoordXSprite();
};

#include "Echiquier.h"
#endif // !defined Piece_h
