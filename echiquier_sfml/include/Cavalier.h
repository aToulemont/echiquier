#ifndef CAVALIER_H
#define CAVALIER_H

#include "Piece.h"

class Cavalier : virtual public Piece
{
public:
    Cavalier(bool white, bool left);
    std::string toString();
    bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
    vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
    std::string codePiece();
    int getCoordXSprite();
};

#endif // CAVALIER_H
