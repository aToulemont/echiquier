#ifndef TOUR_H
#define TOUR_H

#include "Piece.h"

class Tour : virtual public Piece
{
public:
    Tour(bool white, bool left);
    std::string toString();
    bool mouvementValide(Echiquier &e, int colDest, int ligneDest);
    vector<Case*> casesBloquantes(Echiquier &e, int colDest, int ligneDest);
    std::string codePiece();
    int getCoordXSprite();
};

#endif // TOUR_H
