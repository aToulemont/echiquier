#include "../../include/Cavalier.h"
#include <stdlib.h>
#include <sstream>

Cavalier::Cavalier(bool white, bool left) : Piece(left?2:7, white?1:8,white) { }

string
Cavalier::toString()
{
    string couleur = isWhite() ? "blanc" : "noir";

    ostringstream xss, yss;
    xss << x();
    yss << y();
    string coordonnees = "(" + xss.str() + "," + yss.str() + ")";

    return "Cavalier " + couleur + " " + coordonnees;
}

int
Cavalier::getCoordXSprite()
{
    return widthSprite * 2;
}

bool Cavalier::mouvementValide(Echiquier &e, int colDest, int ligneDest)
{
    bool caseAccessible = Piece::caseAlliee(e, colDest, ligneDest);
    int colonneCourante = this->x(), ligneCourante = this->y();

    if(caseAccessible)
    {
        if (colonneCourante == colDest || ligneCourante == ligneDest)
            return false;

        if (abs(colonneCourante-colDest) == 1 && abs(ligneCourante-ligneDest) == 2)
            return true;

        if (abs(colonneCourante-colDest) == 2 && abs(ligneCourante-ligneDest) == 1)
            return true;
    }

    return false;
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Cavalier::casesBloquantes(Echiquier &e, int colDest, int ligneDest)
{
    int colonneCourante = this->x();
    int ligneCourante = this->y();

    int distanceLigne = abs(ligneCourante-ligneDest);
    int distanceColonne = abs(colonneCourante-colDest);

    vector<Case*> itineraire;

    if (distanceColonne == 1 && distanceLigne == 2)
        itineraire.push_back(new Case(colDest,ligneDest));

    if (distanceColonne == 2 && distanceLigne == 1)
        itineraire.push_back(new Case(colDest,ligneDest));

    return itineraire;
}

string Cavalier::codePiece()
{
    return (m_white)?"\xe2\x99\x9e":"\xe2\x99\x98";
}
