#include "../../include/Piece.h"
#include "../../include/Cavalier.h"
#include "../../include/Echiquier.h"

#include <iostream>
#include <stdlib.h>
#include <sstream>

using namespace std;

/** CONSTRUCTEURS **/
Piece::Piece() { }

Piece::Piece(int x, int y, bool white)
{
    m_x = x;
    m_y = y;
    m_white = white;
}

/** DESCTRUCTEUR **/
Piece::~Piece() { }

/** ACCESSEURS**/
int Piece::x()
{
    return m_x;
}

int Piece::y()
{
    return m_y;
}

bool Piece::isWhite()
{
    return m_white;
}

bool Piece::isBlack()
{
    return !m_white;
}

int
Piece::getWidthSprite()
{
    return widthSprite;
}

string
Piece::codePiece()
{
    return (m_white) ? "B" : "N";
}

//Retourne à quelle position (x) est l'image
//correspondant à la pièce dans le sprite
int
Piece::getCoordXSprite()
{
    return 0;
}

/** TO STRING **/
string
Piece::toString()
{
    string couleur = isWhite() ? "blanche" : "noire";

    ostringstream xss, yss;
    xss << x();
    yss << y();
    string coordonnees = "(" + xss.str() + "," + yss.str() + ")";

    return "Piece " + couleur + " " + coordonnees;
}

/** OPERATEUR **/
Piece & Piece::operator=(const Piece & autre)
{
    m_x = autre.m_x;
    m_y = autre.m_y;
    m_white = autre.m_white;
    return *this;
}

/** Initialisation d'une piece **/
void Piece::init(int x, int y, bool white)
{
    m_x = x;
    m_y = y;
    m_white = white;
}

/** Changement de coordonnees d'une piece **/
void Piece::move(int x, int y)
{
    m_x = x;
    m_y = y;
}

/** Affichage des informations d'une piece **/
void Piece::affiche()
{
    cout << "Piece: x=" << m_x << " y=" << m_y << " "
         << (isWhite() ? "blanche" : "noire" ) << codePiece() << endl;
}

/** Détermine le sens de déplacement d'une pièce **/
int Piece::sensDirection (int origine, int dest)
{
    if (origine > dest) return -1;
    else return 1;
}

/** Verifie que la piece effectue un deplacement autorise **/
bool Piece::mouvementValide( Echiquier &e, int colDest, int ligneDest)
{
    return false;
}

/** Retourne les cases parcourues par les pieces mettant le roi en echec **/
vector<Case*> Piece::casesBloquantes(Echiquier &e, int colDest, int ligneDest) { }

/** Verifie que la case de destination est alliee **/
bool Piece::caseAlliee(Echiquier &e, int x, int y)
{
    if (e.getPiece(x,y) != NULL && e.getPiece(x,y)->isWhite() == this->isWhite())
        return false;
    return true;
}
