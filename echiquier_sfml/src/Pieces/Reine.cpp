#include "../../include/Reine.h"

#include <iostream>
#include <stdlib.h>
#include <sstream>

using namespace std;

Reine::Reine(bool white) : Piece(4,white?1:8,white), Tour(white,true), Fou(white,true) { }

string
Reine::toString()
{
    string couleur = isWhite() ? "blanche" : "noire";

    ostringstream xss, yss;
    xss << x();
    yss << y();
    string coordonnees = "(" + xss.str() + "," + yss.str() + ")";

    return "Reine " + couleur + " " + coordonnees;
}

int
Reine::getCoordXSprite()
{
    return widthSprite * 5;
}

bool Reine::mouvementValide(Echiquier &e, int colDest, int ligneDest)
{
    return (Fou::mouvementValide(e,colDest,ligneDest) || Tour::mouvementValide(e,colDest,ligneDest));
}

/** Retourne l'itinéraire de déplacement de la pièce **/
vector<Case*> Reine::casesBloquantes(Echiquier &e, int colDest, int ligneDest)
{
    vector<Case*> itineraire;
    vector<Case*> itineraireFou;
    vector<Case*> itineraireTour;

    itineraireFou = Fou::casesBloquantes(e,colDest,ligneDest);
    itineraireTour = Tour::casesBloquantes(e,colDest,ligneDest);

    vector<Case*>::iterator caseFou = itineraireFou.begin();
    while(caseFou != itineraireFou.end())
    {
        itineraire.push_back(*caseFou);
        caseFou++;
    }

    vector<Case*>::iterator caseTour = itineraireTour.begin();
    while(caseTour != itineraireTour.end())
    {
        itineraire.push_back(*caseTour);
        caseTour++;
    }

    return itineraire;
}

string Reine::codePiece()
{
    return (m_white)?"\xe2\x99\x9b":"\xe2\x99\x95";
}
