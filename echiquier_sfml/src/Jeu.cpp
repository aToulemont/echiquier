#include "Jeu.h"

Jeu::Jeu()
{
    //ctor
}

Jeu::~Jeu()
{
    //dtor
}

sf::RenderWindow* Jeu::getWindow()
{
    return window;
}

void Jeu::init()
{
    jblanc.placerPieces(e);
    jnoir.placerPieces(e);
    joueurCourant = "BLANC";

    initFonts();
    initTextes();
    initWindow();
}

void Jeu::initFonts()
{
    if(!littlelo.loadFromFile("Fonts/LITTLELO.TTF"))
        cout << "erreur lors du chargement de la font Littlelo" << endl;
}

void Jeu::initTextes()
{
    titre.setFont(littlelo);
    titre.setString("Jeu d'echecs");
    titre.setCharacterSize(200);
    titre.setColor(sf::Color::White);
    titre.setPosition(150, 100);

    continuer.setFont(littlelo);
    continuer.setString("Presser la touche espace pour continuer...");
    continuer.setCharacterSize(50);
    continuer.setColor(sf::Color::White);
    continuer.setPosition(150, 350);

    selectionneText.setFont(littlelo);
    selectionneText.setCharacterSize(30);
    selectionneText.setColor(sf::Color::White);
    selectionneText.setPosition(510, 150);

    joueurCourantText.setFont(littlelo);
    joueurCourantText.setCharacterSize(30);
    joueurCourantText.setColor(sf::Color::White);
    joueurCourantText.setPosition(510, 100);

    mouvementInvalideText.setFont(littlelo);
    mouvementInvalideText.setCharacterSize(40);
    mouvementInvalideText.setColor(sf::Color::Red);
    mouvementInvalideText.setPosition(50, 500);

    echecText.setFont(littlelo);
    echecText.setCharacterSize(40);
    echecText.setColor(sf::Color::Red);
    echecText.setPosition(510, 200);

    quitterText.setFont(littlelo);
    quitterText.setCharacterSize(30);
    quitterText.setColor(sf::Color::White);
    quitterText.setPosition(25, 550);
    quitterText.setString("Appuyer sur Echap pour quitter.");

    vainqueurText.setFont(littlelo);
    vainqueurText.setCharacterSize(100);
    vainqueurText.setColor(sf::Color::Red);
    vainqueurText.setPosition(100, 150);
}

void Jeu::initWindow()
{
    window = new sf::RenderWindow(sf::VideoMode(800, 600), "Echiquier - Toulemont & Guedon");
    window->clear(sf::Color(105, 45, 45, 255));
}

void Jeu::menu()
{
    window->draw(titre);
    window->draw(continuer);
    window->draw(quitterText);
    window->display();
}

int Jeu::eventQuitter(sf::Event event)
{
    switch(event.type)
    {
    case sf::Event::KeyPressed:
        if(event.key.code == sf::Keyboard::Escape)
            window->close();
        break;
    case sf::Event::Closed:
        window->close();
        break;
    default:
        break;
    }
}

void Jeu::enrCaseSelectionnee(Piece *p, int colonneCourante, int ligneCourante)
{
    //On enregistre cette pièce dans la pièce courante
    //Si elle appartient au joueur actif
    if((jBlancActif && jblanc.piecePresente(*p)) ||
            (!jBlancActif && jnoir.piecePresente(*p)))
    {
        pSelectionnee = e.getPiece(colonneCourante, ligneCourante);
        ligneSelectionnee = ligneCourante;
        colonneSelectionnee = colonneCourante;

        //Affichage pièce selectionnée
        selectionneText.setString("Pion selectionne : " + pSelectionnee->toString());
    }
}

void Jeu::verifMiseEnEchec(Piece *enleveeCaseDest, Piece *enleveeCaseOrigine, int colonneCourante, int ligneCourante)
{
    Roi* r = !jBlancActif ? jnoir.getRoi() : jblanc.getRoi();
    vector<Piece*> piecesJoueurAdvers = !jBlancActif ? jblanc.getPieces() : jnoir.getPieces();

    if (!r->echec(r->x(), r->y(), piecesJoueurAdvers, e).empty())
    {
        tourTermine = false;
        // Oui, echec, on annule le mouvement
        e.remettrePiece(enleveeCaseDest);

        if (jBlancActif) jnoir.remettrePiece(enleveeCaseDest);
        else jblanc.remettrePiece(enleveeCaseDest);

        // On remet à sa place la pièce déplacée
        pSelectionnee->move(colonneSelectionnee, ligneSelectionnee);
        e.remettrePiece(enleveeCaseOrigine);

        e.enleverPiece(colonneCourante, ligneCourante);

        mouvementValide = false;
        if (echec)
            mouvementInvalideText.setString("Vous ne pouvez pas rester en échec. Rejouez.");
        else
            mouvementInvalideText.setString("Vous ne pouvez pas vous mettre en échec. Rejouez.");
    }
    else
    {
        tourTermine = true;
        /* Le roi adverse est en échec ? */
        Roi* r = jBlancActif ? jnoir.getRoi() : jblanc.getRoi();
        vector<Piece*> piecesJoueurActif = jBlancActif ? jblanc.getPieces() : jnoir.getPieces();
        vector<Piece*> piecesJoueurAdvers = !jBlancActif ? jblanc.getPieces() : jnoir.getPieces();

        // Si au moins une pièce met le roi en échec
        if (!r->echec(r->x(), r->y(), piecesJoueurActif, e).empty())
        {
            echec = true;

            if(r->echecEtMat(piecesJoueurAdvers, e))
                perdu = true;
        }
        else echec = false;
    }
}

void Jeu::mouvement(int colonneCourante, int ligneCourante)
{
    //Si la case courante est différente de celle de la pièce sélectionnée
    if(colonneCourante != colonneSelectionnee || ligneCourante != ligneSelectionnee)
    {
        /* Vérification du mouvement valide */
        if(pSelectionnee->mouvementValide(e, colonneCourante, ligneCourante))
        {
            mouvementValide = true;

            /* On enlève la pièce qui est sur la case de destination */
            Piece* enleveeCaseDest;
            enleveeCaseDest = e.enleverPiece(colonneCourante, ligneCourante);

            if (jBlancActif) jnoir.enleverPiece(*enleveeCaseDest);
            else jblanc.enleverPiece(*enleveeCaseDest);

            /* MAJ des coordonnées de la pièce sélectionnée */
            pSelectionnee->move(colonneCourante, ligneCourante);

            /* Déplacement sur l'échiquier */
            e.placer(pSelectionnee);

            /* Suppression de la pièce de sa case d'origine */
            Piece* enleveeCaseOrigine;
            enleveeCaseOrigine = e.enleverPiece(colonneSelectionnee, ligneSelectionnee);

            /* Le joueur courant se met en échec ? */
            verifMiseEnEchec(enleveeCaseDest, enleveeCaseOrigine, colonneCourante, ligneCourante);

            if(tourTermine)
                pSelectionnee = NULL;
        }
        else
        {
            mouvementValide = false;
            mouvementInvalideText.setString("Mouvement invalide !");
        }
    }
}

void Jeu::terminerTour()
{
    jBlancActif = !jBlancActif;

    //Affichage roi en echec
    string roiEchec = jBlancActif ? "BLANC" : "NOIR";
    if(echec && !perdu)
        echecText.setString("Roi " + roiEchec + " en echec !");

    //Affichage joueur courant
    joueurCourant = jBlancActif?"BLANC":"NOIR";
    joueurCourantText.setString("Au tour du joueur " + joueurCourant);

    tourTermine = false;
}

void Jeu::majAffichage()
{
    window->clear(sf::Color(105, 45, 45, 255));

    e.afficheSFML(window);
    window->draw(joueurCourantText);
    window->draw(selectionneText);
    window->draw(quitterText);
    if(!mouvementValide)
        window->draw(mouvementInvalideText);
    if(echec)
        window->draw(echecText);
}

void Jeu::affichageVainqueur()
{
    window->clear(sf::Color(105, 45, 45, 255));

    joueurCourant = jBlancActif ? "BLANC" : "NOIR";
    vainqueurText.setString("Vainqueur : " + joueurCourant);
    window->draw(vainqueurText);

    quitterText.setPosition(300, 350);
    window->draw(quitterText);
}

void Jeu::jeu()
{
    window->clear(sf::Color(105, 45, 45, 255));
    //Affichage de l'échiquier
    e.afficheSFML(window);
    window->draw(quitterText);

    sf::Event event;
    while (window->pollEvent(event))
    {
        eventQuitter(event);

        if(event.type == sf::Event::MouseButtonPressed && sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            // On récupère les coordonnée de la souris
            sf::Vector2i position = sf::Mouse::getPosition(*window);
            xSouris = position.x;
            ySouris = position.y;

            mouseClicked = true;
        }
    }

    /** Début de la partie **/
    if(!perdu)
    {
        /** Début du tour **/
        if(!tourTermine)
        {
            joueurCourantText.setString("Au tour du joueur " + joueurCourant);

            if(mouseClicked)
            {
                /* Conversion des coordonnées de la souris
                   pour trouver la pièce correspondante */
                int width = e.getWidth();
                int colonneCourante = xSouris/width - 1;
                int ligneCourante = ySouris/width - 1;

                //Si les coordonnées correspondent à l'échiquier
                if(0<colonneCourante && colonneCourante<=8 && 0<ligneCourante && ligneCourante<=8)
                {
                    Piece *p = e.getPiece(colonneCourante, ligneCourante);

                    /// Enregistrement pièce à déplacer ///
                    if(pSelectionnee == NULL && p != NULL)
                        enrCaseSelectionnee(p, colonneCourante, ligneCourante);

                    /// Déplacement de la pièce sélectionnée ///
                    else if(pSelectionnee != NULL)
                        mouvement(colonneCourante, ligneCourante);
                }
                mouseClicked = false;
            }
        }
        /** Tour terminé **/
        else    terminerTour();

        majAffichage();
    }
    /** Jeu terminé **/
    else    affichageVainqueur();

    //MAJ de la fenêtre
    window->display();
}
