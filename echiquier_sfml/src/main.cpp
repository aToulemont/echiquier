#include "Jeu.h"

/**
 * Programme principal
 */
int main( int argc, char** argv )
{
    bool jouer = false;

    Jeu j;
    j.init();

    while (j.getWindow()->isOpen())
    {
        //Action pour quitter
        sf::Event event;
        while (j.getWindow()->pollEvent(event))
            j.eventQuitter(event);

        /// MENU ///
        if(!jouer)
        {
            j.menu();
            while (j.getWindow()->pollEvent(event))
            {
                j.eventQuitter(event);
                if(event.type == sf::Event::KeyPressed)
                    if(event.key.code == sf::Keyboard::Space)
                        jouer = true;
            }
        }
        /// JEU ///
        else    j.jeu();
    }

}
